# README #

This repository is for class materials for Computational Topology, Spring 2018, taught by Prof. Fasy.

### What is in this repository?

The folders in this repository contain all materials for this class.

- board_pics: Photos of the board during lecture.
- handouts: Handouts distributed during lecture.
- hw: The LaTex files with the homework assignments, as well as a template for your submissions. 

The schedule is at the bottom of this Markdown file.

## When and Where?

When? TH 12:15 - 13:30  
Where? Roberts 412

## How do I contact you?

brittany.fasy@montana.edu

Office hours: T,H 16:10 - 17:00

## Accessing this Repo

The repository is set as public, so you can access all course materials easily.
I suggest creating a fork, so that you can use your fork to maintain your own
materials for this class.  See the resources below for forking directions.

To clone this repo:
```
$ git clone https://brittany@bitbucket.org/brittany/comptop-s18.git
```

## Discussions and Questions

Group discussions, questions, and announcements will be through the TDA @ MSU
slack group.  Please subscribe to the 535-sp18 channel.

## Course Outcomes and Objectives
By the end of this course, a student:

- will be able to articulate, both orally and in writing, mathematical 
proofs.
- will demonstrate teamwork skills.
- will demonstrate ability to present and to critique applications of 
research in computational topology.
- will recognize potential applications of TDA.

## Grading
Your grade for this class will be determined by:

- 45% Homework
- 35% Group Project
- 25% Resubmissions and Quizzes

A grade above an 85 will earn at least an A-, above a 70 will earn at least a 
B-, and at least 50 is needed to pass.

## Policy on Assignments

All assignments must be submitted by 23:59 on the due date. Late
assignments will only be accepted if grading has not yet started.

For descriptive assignments and reports, the submission should be typeset in
LaTex, and
submitted as a PDF. For code assignments,
well organized source code with clear comments should be submitted.

## Policy on Collaboration
Collaboration is encouraged on all aspects of the class, except where explicitly 
forbidden. Note:

- All collaboration (who and what) must be clearly indicated in writing on 
anything turned in.  
- Homeworks may be solved collaboratively except as explicitly forbidden, 
but solutions must be written up **independently**. 
This is best done by writing your solutions when not in a group setting.
Groups should be small 
enough that each member plays a significant role.
- For the project, every collaborator must contribute significantly.  How 
the work is divided is at the discretion of the group.

## Policy on Academic Integrity

The integrity of the academic process requires that credit be given where credit
is due. Accordingly, it is academic misconduct to present the ideas or works of
another as one's own work, or to permit another to present one's work without
customary and proper acknowledgment of authorship. Students may collaborate with
other students only as expressly permitted by the instructor. Students are
responsible for the honest completion and representation of their work, the
appropriate citation of sources and the respect and recognition of others'
academic endeavors.

Plagiarism will not be tolerated in this course. According to the Meriam-Webster
dictionary, plagiarism is `the act of using another person's words or ideas
without giving credit to that person.'  Proper credit means describing all
outside resources (conversations, websites, etc.), and explaining the extent to
which the resource was used.  Penalties for plagiarism at MSU include (but are
not limited to) failing the assignment, failing the class, or having your degree
revoked.  This is serious, so do not plagiarize.
Even inadvertent or unintentional misuse or appropriation of another's work
(such as relying heavily on source material that is not expressly acknowledged)
is considered plagiarism. 

By participating in this class, you agree to abide by the Student Code of
Conduct.  This includes the following academic expectations:

- be prompt and regular in attending classes;
- be well-prepared for classes;
- submit required assignments in a timely manner;
- take exams when scheduled, unless rescheduled under 310.01;
- act in a respectful manner toward other students and the instructor and in a way
          that does not detract from the learning experience; and
- make and keep appointments when necessary to meet with the instructor. 

## Policy on Class Attendance

Class attendance is required. If a student must miss a class, please notify the
instructor at least 24 hours in advance.

## Policy on Class Attendance

Class attendance is required. If a student must miss a class, please notify the
instructor at least 24 hours in advance.

## Classroom Etiquette

Except for note taking and group work requiring a computer, please keep electronic devices off during
class, they can be distractions to other students. Disruptions to the class will
result in you being asked to leave the lecture and will negatively impact your
grade.

## Withdrawing

After 28 Februrary 2018, I will only support requests to withdraw from this course
with a ``W" grade if extraordinary personal circumstances exist.
If you are considering withdrawing from this class, discussing this with me as 
early as possible is advised.  Since this class involves a project, the 
decision to withdraw must be discussed with me, and with your group.

## Special needs information

If you have a documented disability for which you are or may be requesting an
accommodation(s), you are encouraged to contact me and Disabled
Student Services as soon as possible.

## Resources

### Technical Resources

- [Git Udacity
  Course](https://www.udacity.com/course/how-to-use-git-and-github--ud775)
- [Forking in Git](https://help.github.com/articles/fork-a-repo/)
- [Markdown](http://daringfireball.net/projects/markdown/)
- [Inkscape Can Tutorial](http://tavmjong.free.fr/INKSCAPE/MANUAL/html/SoupCan.html)
- [Plagiarism Tutorial](http://www.lib.usm.edu/legacy/plag/pretest_new.php)]
- [Ott's 10 Tips](http://www.ms.uky.edu/~kott/proof_help.pdf)

### Main References

- [Edelsbrunner and Harer, Computational
  Topology](http://bookstore.ams.org/mbk-69) - Our course textbook.
- [Munkres, Topology](http://www.maths.ed.ac.uk/~aar/papers/munkres2.pdf)

### Additional References

- [Big-Oh, Intuitive Explanation](https://rob-bell.net/2009/06/a-beginners-guide-to-big-o-notation/)
- [CLRS, Introduction to
  Algorithms](https://mitpress.mit.edu/books/introduction-algorithms): this book
  is THE algorithms textbook.
  * [Master's Method](handouts/clrs-master.pdf)
- [Master's
  Method](http://www.cse.unt.edu/~tarau/teaching/cf1/Master%20theorem.pdf)

### Videos
- [Carlsson Short](https://www.youtube.com/watch?v=XfWibrh6stw)

## Schedule

### Week 1 (9 January 2018)
- Topic: Introduction to Topology: Sets and Functions
- Suggested Reading: [Munkres, Topology](http://www.maths.ed.ac.uk/~aar/papers/munkres2.pdf), Chapter 1
- HW-01 assigned. Due 1/16.  Location: comptop-s18/hw/H-01/

### Week 2 (16,18 January 2018)
- Topics: Analysis of Algortihms, Graphs, Topology and Neighborhoods
- Required Reading: [EH, Computational
  Topology](http://bookstore.ams.org/mbk-69), Chapter 1
- Suggested Reading: [Munkres, Topology](http://www.maths.ed.ac.uk/~aar/papers/munkres2.pdf), Chapter 2
- HW-02 assigned. Due 1/30. Location: comptop-s18/hw/H-02/
- **Due this week: H-01 (Tues)**

### Week 3 (23,25 January 2018)
- Topics: Point Set Topology and Curves
- Required Reading: [EH, Computational
  Topology](http://bookstore.ams.org/mbk-69), Section I.3
- Suggested Reading: [Ault, Understanding
  Topology](https://www.press.jhu.edu/news/blog/teaching-topology-preview-shaun-aults-understanding-topology),
  Chapter 4;  [Sullivan, Curves of
  Finite Total Curvature](https://arxiv.org/abs/math/0606007)
- *Due this week: Q-01 (Thurs)*

### Week 4 (30 Jan / 1 Feb 2018)
- Topics: Surfaces
- Required Reading: [EH, Computational
  Topology](http://bookstore.ams.org/mbk-69), Section II.1
- *Due this week: H-02 (Tues), P-0 (Tues)*

--- 

This syllabus was created, using wording from previous courses that I have
taught, as well as David Millman's Spring 2018 Graphics course.  Thanks, Dave!
